from flask import Flask
import function_utilities as functions
import db_connection as db
from flask import request
import json
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

from datetime import datetime

def validate(date_text):
    try:
        if date_text != datetime.strptime(date_text, "%m-%d-%Y").strftime('%Y-%m-%d'):
            raise ValueError
        return True
    except ValueError:
        return False
def build_query(fc_number, selected_date):
    # query = 'SELECT * FROM SCM_FR.PO_SBG_TRANSFERS WHERE Destination_FC = {} AND Date_Picked={}'.format(fc_number, selected_date)
    return query

@app.route('/fc_details/', methods=['GET'])
def retrieve_data():
    if request.method == 'GET':
        selected_date = int(request.args.get('fc'))
        selected_fc = request.args.get('date')
        query = query = 'SELECT * FROM SCM_FR.PO_SBG_TRANSFERS WHERE Destination_Fc ={} AND Picked_Date={}'.format(selected_fc,selected_date)
        #crsr = db.remote_connection()
        #data = db.get_data(query,  crsr)
        print(selected_date,selected_fc)
        with open('json.json') as f:
            result = json.load(f)
            data = json.dumps(result)

    return data

@app.route('/update', methods=['POST'])
def update_data():
    
    if request.method == 'POST':
        data = request.get_json()
        PO = data['PO']
        SHIPMENT_NBR = data['SHIPMENT_NBR']
        BOL = data['BOL']
        Staples_BOL = data['Staples_BOL']
        Source_FC = data['Source_FC']
        Destination_FC = data['Destination_FC']
        Pallets = data['Pallets']
        Trailer_Number = data['Trailer_Number']
        SHIPMENT_DATE = data['SHIPMENT_DATE']
        Date_Routed = data['Date_Routed']
        Date_Picked = data['Date_Picked']
        PO_Type = data['PO_Type']
        #selected_date = request.query_params['Date_Picke']
        #selected_fc = request.query_params['selected_fc']
        query = query = 'SELECT * FROM SCM_FR.PO_SBG_TRANSFERS'
        #crsr = db.remote_connection()
        #data = db.get_data(query,  crsr)
        print(PO,SHIPMENT_NBR,BOL,Staples_BOL,Date_Routed)
    #print(data)

    return '{"message": "successfully"}'

if __name__ == "__main__":
    app.run()