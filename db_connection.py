import teradata
import pyodbc
import list_of_constants as constant

def remote_connection():
    host = 'td-gdw-p-'
    link = 'DRIVER=Teradata Database ODBC Driver 16.00;DBCNAME={};UID={};PWD={}'.format(host, constant.USERNAME,constant.PASSWORD)
    session = pyodbc.connect(link, autocommit =  True)
    return session